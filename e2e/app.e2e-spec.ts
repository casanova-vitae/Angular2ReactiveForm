import { ZebraAppPage } from './app.po';

describe('zebra-app App', function() {
  let page: ZebraAppPage;

  beforeEach(() => {
    page = new ZebraAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('zb works!');
  });
});
