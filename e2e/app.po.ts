import { browser, element, by } from 'protractor';

export class ZebraAppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('zb-root h1')).getText();
  }
}
