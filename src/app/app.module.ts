import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { AppComponent } from './app.component';
import { FormTemplateComponent } from './form-template/form-template.component';
import { FormDataComponent } from './form-data/attribute-list/form-data.component';
import { DatacenterService } from './form-data/attribute-list/datacenter.service';
import  {LocalDatacenterService} from './form-data/attribute/local-datacenter.service'
import { AttributeFormComponent } from './form-data/attribute/attribute-form.component'
import {routing} from "./app.routing";
import { FilterPipe } from './form-data/attribute-list/filter.pipe';
import { AttributeDirective } from './form-data/attribute/attribute.directive';

@NgModule({
  declarations: [
    AppComponent,
    FormTemplateComponent,
    FormDataComponent,
    AttributeFormComponent,
    FilterPipe,
    AttributeDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    MaterialModule.forRoot(),
    routing
  ],
  providers: [DatacenterService,LocalDatacenterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
