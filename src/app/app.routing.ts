import {Routes, RouterModule} from "@angular/router";
import {AppComponent} from "./app.component";
import {FormDataComponent} from "./form-data/attribute-list/form-data.component";
const APP_ROUTES: Routes = [
  {path: 'metadata', component: FormDataComponent},
  {path: 'data', component: FormDataComponent},
  {path: '', component: FormDataComponent}

];
export const routing = RouterModule.forRoot(APP_ROUTES);
