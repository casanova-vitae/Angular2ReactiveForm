export class Attribute {
  constructor(public name:string, public description:string, public imagePath:string, public vnum:string[]){
  }
}
