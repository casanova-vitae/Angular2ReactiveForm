import { Injectable } from '@angular/core';
import { Attribute } from "./data";
@Injectable()
export class DatacenterService {
  private attributes: Attribute[]=[
    new Attribute('B1','B2','B3',['u1','u2']),
    new Attribute('B1','B2','B3',['c1','c2']),
    new Attribute('B1','B2','B3',['d1','d2'])
  ]
  constructor() {}
  getAttribute(){
    return this.attributes;
  }
  addAttribute(attributes: Attribute){
    this.attributes.push(attributes)
  }
  removeAttribute(index: number){
    this.attributes.splice(index, 1);
  }
  removeSubAttribute(subIndex: number){
  }

}
