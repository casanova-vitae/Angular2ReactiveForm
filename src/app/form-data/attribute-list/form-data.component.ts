import { Component, OnInit,Input } from '@angular/core';
import {Localattribute} from '../attribute/localattribute';
import {LocalDatacenterService} from '../attribute/local-datacenter.service';
import { ActivatedRoute ,Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';
@Component({
  selector: 'data-driven',
  templateUrl: 'form-data.component.html'
})
export class FormDataComponent implements OnInit{
  public arrayValidation = [];
  public attrRoute;
  public boolFlag:boolean;
  attributes: Localattribute[] = [];
  auxattr: Localattribute[] = [];
  attr: Localattribute;
  constructor(private attributeService: LocalDatacenterService,private route: ActivatedRoute,urouter: Router){
    console.log(urouter.url);
    if(urouter.url == '/'){
      this.attrRoute = '/data';
    }else{
      this.attrRoute = urouter.url;
    }

    Observable.interval(2000).subscribe(x => {
      this.boolFlag =  !(this.reviewDuplicate() && this.reviewWellFormed()) //Validates non name duplicity and Well Formed Form
    });
  }
  ngOnInit() {
    this.attributes = this.attributeService.getAttribute();//Get Array of type Localattribute and load Component Attribute Instances
    this.boolFlag =  !(this.reviewDuplicate() && this.reviewWellFormed())
  }
  reviewDuplicate(): boolean {
    let parentharray = this.attributes;
    let array = this.attributes;
    for (let item of parentharray){
      let itemName = item.name;
      let count = 0;
      for (let subItem of array){
        let subItemName = subItem.name;
        if(itemName == subItemName){
          count ++;
        }
      }
      if(count > 1) {
       return false;
      }
    }
    return true;
  }
  reviewWellFormed(): boolean {
    let parentharray = this.attributes;
    let array = this.attributes;
    for (let item of this.attributes){
      //if(item.statusForm){console.log(item.statusForm);}else{console.log("FALSEEEEEE");}
      if(!item.statusForm){return false}
    }
    return true;
  }

  onAddItem()
  {
    this.attr = new Localattribute('DEFAULT','DEFAULT','DEFAULT','DEFAULT','DEFAULT','DEFAULT',['DF1','DF2'],0.5,0.5,'mm',0.5,0.5,this.attrRoute,false);
    this.attributeService.addAttribute(this.attr);
  }
  handleUserUpdated(user:string) {

  }

}
