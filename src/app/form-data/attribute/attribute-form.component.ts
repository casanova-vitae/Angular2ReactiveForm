import { Component, OnInit,Input,Output, EventEmitter } from '@angular/core';
import {  FormGroup,  FormControl,  Validators,  FormBuilder,} from "@angular/forms";
import {LocalDatacenterService} from './local-datacenter.service';
import {Localattribute} from './localattribute';
import { Observable } from "rxjs/Rx";
import { matchingRange} from './validators'
import { AttributeDirective } from './attribute.directive';

@Component({
  selector: 'zb-attribute-form',
  templateUrl: './attribute-form.component.html',
  styleUrls: ['./attribute-form.component.css']
})

export class AttributeFormComponent implements OnInit {
  myForm: FormGroup;

  @Output() userUpdated = new EventEmitter<string>();
  @Input() parentObject: Localattribute;
  @Input() reciveIndex = 0;

  attributes: Localattribute[] = [];

  public types; public formats; public enumValidation:boolean = false;
  public label_display; public label_string; public output;
  public edited;  public flagrange = false; public flagenum = true;

  constructor(private formBuilder: FormBuilder,private attributeService: LocalDatacenterService){
    this.edited = true;
    this.myForm = this.formBuilder.group({
        'name':         [{value: '' , disabled: false}, [Validators.required]],
        'description':  [{value:''  , disabled: false}, [Validators.required]],
        'device':       [{value:''}, [Validators.required]],
        'default':      [{value:''  , disabled: false}, [Validators.required]],
        'datatype':     [{value:''  , disabled: false},],
        'format':       ['', []],
        'enumeration':  ['Enum'],
        'rangemin':     ['', [Validators.required]],
        'rangemax':     ['', [Validators.required]],
        'uom':          ['',],
        'precision':    ['', [Validators.required]],
        'accuracy':     ['', [Validators.required]],
      },
        {
          validator:matchingRange('rangemin','rangemax','precision','accuracy')
        }
      );

    this.myForm.valueChanges.subscribe(data => {
      this.attributeService.updateFormStatus(this.reciveIndex,this.myForm.valid);
      this.output = data
    })

    this.myForm.controls['name'].valueChanges.subscribe(data => {
        this.attributeService.updateAttribute(this.reciveIndex,data);
     // }
    })

    Observable.interval(4000).subscribe(x => {
      if(this.myForm.valid){this.userUpdated.emit('VALID');}else{this.userUpdated.emit('INVALID');}
    });

  }

  onSubmit() {
    console.log(this.myForm);
  }

  enumLenghtValidator(control: FormControl): {[s: string]: boolean} {
    return {example: true};
  }

  exampleValidator(control: FormControl): {[s: string]: boolean} {
    if (control.value === 'Example') {
      return {example: true};
    }
    return null;
  }

  asyncExampleValidator(control: FormControl): Promise<any> | Observable<any> {
    const promise = new Promise<any>(
      (resolve, reject) => {
        setTimeout(() => {
          if (control.value === 'Example') {
            resolve({'invalid': true});
          } else {
            resolve(null);
          }
        }, 1500);
      }
    );
    return promise;
  }

  ngOnInit() {
    this.types = this.attributeService.getTypes();
    this.formats = this.attributeService.getFormats();
    for (let item of this.attributeService.getControlsForm()){
      this.myForm.controls[item[0]].setValue(this.parentObject[item[1]]);
    }

    this.callType();

    this.label_string = this.attributeService.onTransformType(this.parentObject.types);
    this.label_display = this.attributeService.onTransformFormat(this.parentObject.format)

    this.attributeService.getTypes();
    this.attributeService.getFormats();

    if( this.parentObject.listenum.length > 0){
      this.enumValidation = true;
    } else{this.enumValidation = false;}

  }

  onPress(){
    if (this.edited == true)
    {
      this.edited = false;
    }else{this.edited = true;}
  }

  callType(){
    console.log("CallType");
    let name=this.myForm.controls['name'];
    let description =this.myForm.controls['description'];
    let device = this.myForm.controls['device'];
    let def = this.myForm.controls['default'];
    let rangemin=this.myForm.controls['rangemin'];
    let rangemax=this.myForm.controls['rangemax'];
    let uom=this.myForm.controls['uom'];
    let precision=this.myForm.controls['precision'];
    let accuracy=this.myForm.controls['accuracy'];
    let type = this.myForm.controls['datatype'];
    let form = this.myForm.controls['format'];

    if (type.value == 1){
      device.disable(true);
    }
    if (type.value == 1 && form.value == 1){
      this.flagrange = false;
      this.flagenum = true;
      rangemin.disable(true);
      rangemax.disable(true);
      uom.disable(true);
      precision.disable(true);
      accuracy.disable(true);

      /*this.values[this.controls[i].name]=undefined;
      this.form.removeControl(this.controls[i].name);
      this.controls.splice(i);
      this.form.controls.forEach(c => c.updateValueAndValidity());*/
    }
    if (type.value == 1 && form.value == 2){
      this.flagrange = true;
      this.flagenum =false;
      rangemin.enable(true);
      rangemax.enable(true);
      uom.enable(true);
      precision.enable(true);
      accuracy.enable(true);
    }
    if (type.value == 1 && form.value > 2){
      this.flagrange = false;
      this.flagenum =false;
    }
    if (type.value == 2 && form.value > 0){
      this.flagrange = false;
      this.flagenum =false;
      device.disable(true);
      def.disable(true);
      form.disable(true);
    }else{
      //device.enable(true);
      def.enable(true);
      form.enable(true);
    }
  }

  onChangeDataRange(value,type){
    console.log(value,type);
  }

  onDelete(){
    this.attributeService.removeAttribute(this.reciveIndex);
  }

  onDeleteSubItem(num: number){
    this.parentObject.listenum.splice(num, 1);
  }

  onAddSubItem(){
    let value = this.myForm.controls['enumeration'].value;
    var partsOfStr = value.split(',');
    if(value.indexOf(",") >= 0){
      let partsOfStr = value.split(',');
      for (let valueItem of partsOfStr){
        this.parentObject.listenum.push(valueItem)
      }
    }else{
    this.parentObject.listenum.push(this.myForm.controls['enumeration'].value)
    }
  }


}
