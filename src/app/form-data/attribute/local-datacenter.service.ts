import { Injectable } from '@angular/core';
import {Localattribute} from './localattribute'
@Injectable()
export class LocalDatacenterService {
  private attributes: Localattribute[]=[
    new Localattribute('DEFAULT','DEFAULT','DEFAULT','DEFAULT','1','1',['DF1','DF2'],0.5,5,'mm',0.5,0.5,'/data',false),
    new Localattribute('DEFAULT','DEFAULT','DEFAULT','DEFAULT','1','1',['DF1','DF2'],0.5,0.5,'mm',0.5,0.5,'/data',false),
    new Localattribute('DEFAULT','DEFAULT','DEFAULT','DEFAULT','1','1',['DF1','DF2'],0.5,0.5,'mm',0.5,0.5,'/data',false),
    new Localattribute('DEFAULT','DEFAULT','DEFAULT','DEFAULT','1','1',['XF1','MF2'],0.5,0.5,'mm',0.5,0.5,'/metadata',false),
    new Localattribute('DEFAULT','DEFAULT','DEFAULT','DEFAULT','1','1',['XF1','MF2'],0.5,0.5,'mm',0.5,0.5,'/metadata',false)
  ]
  private types = [
    { value: '1', display: 'STRING' },
    { value: '2', display: 'OBJECT' }
  ];
  private formats = [
    { value: '1', display: 'NONE' },
    { value: '2', display: 'NUMBER' },
    { value: '3', display: 'BOOLEAN' },
    { value: '4', display: 'DATE-TIME' },
    { value: '5', display: 'CDATA' },
    { value: '6', display: 'URI' }
  ];

  private controlsForm =[
    ['name','name'],['description','description'],['device','device'],['default','deafault'],['datatype','types'],['format','format'],['rangemin','rmin'],['rangemax','rmax'],['uom','uom'],['precision','presicion'],['accuracy','accuracy']
  ]

  constructor() {}

  getAttribute(){
    return this.attributes;
  }

  getTypes(){
    return this.types;
  }

  getFormats(){
    return this.formats;
  }

  getControlsForm(){
    return this.controlsForm;
  }

  addAttribute(attributes: Localattribute){
    this.attributes.push(attributes)
  }

  removeAttribute(index: number){
    this.attributes.splice(index, 1);
  }

  updateAttribute(index: number, itemValue: string){
    this.attributes[index].name = itemValue;
  }

  updateFormStatus(index: number, formValue: boolean){
    this.attributes[index].statusForm = formValue;
  }


  onTransformFormat(data : string){
    switch(data) {
      case '1':
        return "STRING";
      case '2':
        return "OBJECT";
    }
  }

  onTransformType(data : string){
    switch(data) {
      case '1':
        return "NONE";
      case '2':
        return "NUMBER";
      case '3':
        return "BOOLEAN";
      case '4':
        return "DATE-TIME";
      case '5':
        return "CDATA";
      case '6':
        return "URI";
    }
  }

}
