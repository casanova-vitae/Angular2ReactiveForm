export class Localattribute {

  constructor(public name:string,
              public description:string,
              public device:string,
              public deafault:string,
              public format:string,
              public types:string,
              public listenum:string[],
              public rmin:number,
              public rmax:number,
              public uom:string,
              public presicion:number,
              public accuracy:number,
              public category:string,
              public statusForm: boolean,
  ){
  }
}
