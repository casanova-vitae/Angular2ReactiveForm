import {FormGroup} from "@angular/forms";

export function matchingRange(rangemin: string, rangemax: string,precision: string , accuracy: string) {
  return (group: FormGroup): {[key: string]: any} => {
    let rmin = group.controls[rangemin];
    let rmax = group.controls[rangemax];
    let prec = group.controls[precision];
    let accu = group.controls[accuracy];
    if (rmax.value < rmin.value ) {
    console.log(rmax.value);
      console.log(rmin.value);
      return {
        mismatchedRange: true
      };
    }else{

      if(prec.value >= rmin.value && prec.value <= rmax.value && accu.value >= rmin.value && accu.value <= rmax.value)
      {
        if (prec.value % 1 == 0) {
          let intNumberItem = prec.value;
          let intRmin = rmin.value;
          let intRmax = rmax.value;
          if(!( intRmin % intNumberItem == 0 || intRmax % intNumberItem == 0) ){
            return {
              mismatchedprec: true
            };
          }
        }else{
          let numberItem = prec.value;
          numberItem =numberItem *10;
          numberItem =Math.floor(numberItem );
          numberItem =numberItem /10;
          let intNumberItem = numberItem*10;
          let intRmin = rmin.value*10;
          let intRmax = rmax.value*10;

          if(!( intRmin % intNumberItem == 0 || intRmax % intNumberItem == 0) ){
            return {
              mismatchedprec: true
            };
          }else{
          }
        }

        if (accu.value % 1 == 0) {

          let intNumberItem = accu.value;
          let intRmin = rmin.value;
          let intRmax = rmax.value;
          if(!( intRmin % intNumberItem == 0 || intRmax % intNumberItem == 0) ){
            return {
              mismatchedaccu: true
            };
          }

        }else{

          let numberItem = accu.value;
          numberItem =numberItem *10;
          numberItem =Math.floor(numberItem );
          numberItem =numberItem /10;

          let intNumberItem = numberItem*10;
          let intRmin = rmin.value*10;
          let intRmax = rmax.value*10;

          if(!( intRmin % intNumberItem == 0 || intRmax % intNumberItem == 0) ){
            return {
              mismatchedaccu: true
            };
          }

        }
      }else{
        return {
          mismatchInterval: true
        };
      }
      }
    }
  }

