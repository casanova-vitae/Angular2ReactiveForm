import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import {FormControl} from '@angular/forms';

@Component({
  selector: 'zb-form-template',
  templateUrl: './form-template.component.html',
  styleUrls: ['./form-template.component.css']
})
export class FormTemplateComponent implements OnInit {
  attribute = {
    name: '',
    description: '',
    drt: '',
    dv:'',
    dt:'',
    format:'',
    enum:[],
    min:'',
    max:'',
    uom:'',
    type: '',
  };

  public types = [
    { value: '1', display: 'STRING' },
    { value: '2', display: 'CHAR' },
    { value: '3', display: 'OBJECT' }
  ];



  genders = [
    'male',
    'female'
  ];

  onSubmit(form: NgForm) {
    //console.log(form.value);
    console.log(this.attribute);
  }

  constructor() { }

  ngOnInit() {
  }



}
